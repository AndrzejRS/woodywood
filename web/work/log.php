<!-- tu inkludujemy header z pliku header.inc.php z tego samego katalogu -->
<?php include('header.inc.php') ?>

<!-- tu dajemy w każdej podstronce wszystko co ma się pokazać między headerem i footerem -->

        <div class="wrapper">
            

            <div class="container">
                <div id="customer-login" class="col-xs-offset-3 col-xs-6 col-xs-offset-3">
            
                    <div id="login">
                        
                        <div id="title">
                        <h2>Zaloguj</h2>
                        </div>

                        <form method="post" class="login">  
                            
                            <div class="name">
                                <p class="row-log">
                                <label for="username">Nazwa użytkownika lub e-mail <span class="required"></span></label>
                                <input class="input-text" name="username" id="username" value="" type="text">
                                </p>
                            </div>
                            
                            <div class="password">                        
                                <p class="row-log">
                                <label for="password">Hasło <span class="required"></span></label>
                                <input class="input-text" name="password" id="password" type="password">
                                </p>
                            </div>
                            
                            <div>
                                <p>
                                <input class="button-3" name="login" value="Zaloguj" type="submit">
                                </p>
                            </div>

                            <div class="lost-password">
                                <span><a href="#">Nie pamiętasz hasła?</a></span>
                            </div>
                
                        </form>
                    </div>
                </div>
            </div>

<?php include('footer.inc.php') ?>
