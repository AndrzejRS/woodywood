        <!-- to jest zamknięcie wrappera którego otwieramy w header -->
        </div>
        <!-- tu w środku leci wszystko co ma być w footer -->
        <footer class="container-footer">
       
        <div class="row">
         <button id="up"><span class="glyphicon glyphicon-menu-up"></span></button>
            <nav class="col-xs-12 social">
                <ul>
                    <li>
                    <a href="http://www.facebook.com">
                        <span>
                        <i class="fa fa-facebook" aria-hidden="true"></i>
                        </span>
                    </a>
                    </li>
                    
                    <li>
                    <a href="http://www.twitter.com">
                        <span>
                          <i class="fa fa-twitter" aria-hidden="true"></i>
                        </span>
                    </a>
                    </li>

                    <li>
                    <a href="http://pl.pintrest.com">
                        <span>
                            <i class="fa fa-pinterest-p" aria-hidden="true"></i>
                        </span>
                    </a>
                    </li>
                    
                </ul>
            </nav>
            <div class="col-xs-12">
                <div class="foot-info">
                    <p> &copy Woodywood 2017 <span> design by </span><a href="http://webbit.pl">webbit.pl</a></p>
                </div>
            </div>
        </div>    

        </footer>
        
        <!-- tu ładujemy pozostałe skrypty JS które będą potrzebne, na początek bootstrap i plik do własnych rzeczy main.js -->
        
        <script type="text/javascript" src="../js/bootstrap.js"></script>
        
        <script type="text/javascript" src="../js/vendor/jquery-2.2.4.min.js"></script>
        <!-- ikony social -->
        <script src="https://use.fontawesome.com/47dc507400.js"></script>
        <!-- fancybox do zdjęć w index -->
        <script type="text/javascript" src="../js/vendor/jquery.fancybox.min.js" ></script>
       <script type="text/javascript" src="../js/main.js"></script>
        

        
<!-- zamknięcie body i html koniec strony -->

    </body>
</html>