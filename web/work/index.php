<!-- tu inkludujemy header z pliku header.inc.php z tego samego katalogu -->
<?php include('header.inc.php') ?>

<!-- tu dajemy w każdej podstronce wszystko co ma się pokazać między headerem i footerem -->
<section class="container-fluid">
	<article class="container">

		<!-- slider -->
		<div id="carousel-example-generic" class="carousel slide">

		<!-- kropki do przewijania -->
			<ol class="carousel-indicators visible-lg visible-md">
				<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
				<li data-target="#carousel-example-generic" data-slide-to="1"></li>
				<li data-target="#carousel-example-generic" data-slide-to="2"></li>
			</ol>
		
		<!-- obrazki w slajderze -->	
			<div class="carousel-inner visible-lg visible-md visible-sm">
				
				<div class="item active">
					<img src="../../img/1.jpg" alt="pierwszy slajd">
					<!-- opis slajdu -->
					<div class="carousel-caption">
						<h3>tytuł pierwszego slajdu</h3>
						<p>opis pierwszego slajdu</p>
					</div>				
				</div>

				<div class="item">
					<img src="../../img/2.jpg" alt="drugi slajd">
					<!-- opis slajdu -->
					<div class="carousel-caption">
						<h3>tytuł drugiego slajdu</h3>
						<p>opis drugiego slajdu</p>
					</div>				
				</div>

				<div class="item">
					<img src="../../img/3.jpg" alt="trzeci slajd">
					<!-- opis slajdu -->
					<div class="carousel-caption">
						<h3>tytuł trzeciego slajdu</h3>
						<p>opis trzeciego slajdu</p>
					</div>				
				</div>			 
			</div>
		
		<!-- strzałki do przewijania -->
			<a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
				<span class="icon-prev"></span></a>
			<a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
				<span class="icon-next"></span>
			</a>
		</div>
	</article>
</section>

	<!-- klawisz szukania -->
<section class="container">
	<div class="row"> 
		<form class="col-xs-12">
			<div class="col-xs-12 col-md-6 main-search" role="form">
    	    	<input type="text" placeholder="wpisz szukane słowo">
    	    	<button type="submit" class="button">Szukaj</button>
    	    </div>
	    
	<!-- Klawisz filtrowania -->
			<div class="col-xs-12 col-md-6 main-search">
			<span>Sortuj po: </span>
			<select class="options" name="options">
				<option value="name_up">nazwie rosnąco</option>
				<option value="name_down">nazwie malejąco</option>
				<option value="price_up">cenie rosnąco</option>
				<option value="price_down">cenie malejąco</option>
			</select>
			</div>
		</form>

	</div>
</section>

	<!-- galera zdjęć z opisami -->
<section class="container-fluid gallery">
	<nav class="container">
		<div class="row">
		

<?php for($i=1;$i<=9;$i++): ?>
	<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
				<div class="thumbnail">
					<a href="../../img/chair-big.jpg" data-fancybox data-caption="My caption">
					<img src="../../img/chair-small.jpg" alt="krzesło" />
					</a>
						<div class="caption">
							<h3>Pierwszy produkt</h3>
							<p>opis produktu - Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi non quis exercitationem culpa nesciunt nihil aut nostrum explicabo reprehenderit optio amet ab temporibus asperiores quasi cupiditate. Voluptatum ducimus voluptates voluptas?</p>
							
							<a href="koszyk.php" class="btn btn-primary" role="button">Do koszyka</a>
							</p>
						</div>
				</div>
	</div>
<?php endfor ?>

<!-- 			<div class="col-sm-6 col-md-4">
	<div class="thumbnail">
		<img src="../../img/krzesło.jpg" alt="żółte krzesło" class="img-responsive">
			<div class="caption">
				<h3>Pierwszy produkt</h3>
				<p>opis produktu</p>
				<p><a href="#" class="btn btn-primary" role="button">Powiększ</a>
					<a href="#" class="btn btn-primary" role="button">Do koszyka</a>
				</p>
			</div>
	</div>
</div>

<div class="col-sm-6 col-md-4">
	<div class="thumbnail">
		<img src="../../img/krzesło.jpg" alt="żółte krzesło"  class="img-responsive">
			<div class="caption">
				<h3>Drugi produkt</h3>
				<p>opis produktu</p>
				<p><a href="#" class="btn btn-primary" role="button">Powiększ</a>
					<a href="#" class="btn btn-primary" role="button">Do koszyka</a>
				</p>
			</div>
		
	</div>
</div>

<div class="col-sm-6 col-md-4">
	<div class="thumbnail">
		<img src="../../img/krzesło.jpg" alt="żółte krzesło" class="img-responsive">
			<div class="caption">
				<h3>Drugi produkt</h3>
				<p>opis produktu</p>
				<p><a href="#" class="btn btn-primary" role="button">Powiększ</a>
					<a href="#" class="btn btn-primary" role="button">Do koszyka</a>
				</p>
			</div>
		
	</div>
</div>

<div class="col-sm-6 col-md-4">
	<div class="thumbnail">
		<img src="../../img/krzesło.jpg" alt="żółte krzesło" class="img-responsive">
			<div class="caption">
				<h3>Pierwszy produkt</h3>
				<p>opis produktu</p>
				<p><a href="#" class="btn btn-primary" role="button">Powiększ</a>
					<a href="#" class="btn btn-primary" role="button">Do koszyka</a>
				</p>
			</div>
	</div>
</div>

<div class="col-sm-6 col-md-4">
	<div class="thumbnail">
		<img src="../../img/krzesło.jpg" alt="żółte krzesło"  class="img-responsive">
			<div class="caption">
				<h3>Drugi produkt</h3>
				<p>opis produktu</p>
				<p><a href="#" class="btn btn-primary" role="button">Powiększ</a>
					<a href="#" class="btn btn-primary" role="button">Do koszyka</a>
				</p>
			</div>
		
	</div>
</div>

<div class="col-sm-6 col-md-4">
	<div class="thumbnail">
		<img src="../../img/krzesło.jpg" alt="żółte krzesło" class="img-responsive">
			<div class="caption">
				<h3>Drugi produkt</h3>
				<p>opis produktu</p>
				<p><a href="#" class="btn btn-primary" role="button">Powiększ</a>
					<a href="#" class="btn btn-primary" role="button">Do koszyka</a>
				</p>
			</div>
		
	</div>
</div> -->
		</div>
		<div class="row">
			<nav class="col-xs-12 paginacja">
				<ul class="pagination">
					<li><a href="#">&laquo;</a></li>
					<li><a href="#">1</a></li>
					<li><a href="#">2</a></li>
					<li><a href="#">3</a></li>
					<li><a href="#">&raquo;</a></li>
				</ul>
			</nav>
		</div>
	</nav>
</section>

	<!-- paginacja  -->
<section class="container-fluid">

</section>

<!-- tu inkludujemy footer z pliku footer.inc.php z tego samego katalogu -->
<?php include('footer.inc.php') ?>