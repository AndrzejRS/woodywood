
<!-- tu inkludujemy header z pliku header.inc.php z tego samego katalogu -->
<?php include('header.inc.php') ?>

<!-- tu dajemy w każdej podstronce wszystko co ma się pokazać między headerem i footerem -->

        <div class="wrapper">
           

            <div class="container">
                <div id="customer-login" class="col-xs-offset-3 col-xs-6 col-xs-offset-3">
            
                    <div id="login">
                        
                        <div id="title">
                        <h2>Rejestracja</h2>
                        </div>

                        <form method="post" class="login">  
                            
                            <div class="name">
                                <p class="row-log">
                                <label for="username">E-mail <span class="required"></span></label>
                                <input class="input-text" name="username" id="username" value="" type="text">
                                </p>
                            </div>
                            
                            <div class="password">                        
                                <p class="row-log">
                                <label for="password">Hasło <span class="required"></span></label>
                                <input class="input-text" name="password" id="password" type="password">
                                </p>
                            </div>

                            <div class="password">                        
                                <p class="row-log">
                                <label for="password">Powtórz hasło <span class="required"></span></label>
                                <input class="input-text" name="password" id="password" type="password">
                                </p>
                            </div>                            
                             
                            <div>
                                <form action="#">
                                    <row>
                                        <div class="col-xs-1">
                                            <input type="checkbox" name="agreement" checked="checked" id="agree1" />
                                            <label for="agree1"></label>
                                        </div>

                                        <div class="col-xs-11">
                                            <p>Wyrażam zgodę na przetwarzanie danych osobowych wskazanych przeze mnie na moim koncie Klienta przez administratora danych (WoodyWood, ul.Godebskiego, 20-001 Lublin) zgodnie z ustawą z dnia 29 sierpnia 1997 roku o ochronie danych osobowych w celu niezbędnym do utrzymywania zarejestrowanego przeze mnie konta Klienta. Przysługuje mi prawo dostępu do treści moich danych oraz ich poprawiania. Podanie danych jest dobrowolne, jednakże brak zgody na ich przetwarzanie uniemożliwia rejestrację konta w Sklepie Internetowym WoodyWood.
                                            </p>
                                        </div>
                                    </row>

                                    <row>
                                        <div class="col-xs-1">
                                            <input type="checkbox" name="info" id="agree2"/>
                                            <label for="agree2"></label>
                                        </div>
                                        <div class="col-xs-11">
                                            <p> Wyrażam zgodę na przesyłanie przez WoodyWood informacji handlowych za pomocą środków telekomunikacji elektronicznej w rozumieniu ustawy z dnia 18 lipca 2002 roku o świadczeniu usług drogą elektroniczną. Zgodę tę mogę cofnąć w każdym czasie.
                                            </p>
                                        </div>
                                    </row>
                                
                                    <row>
                                        <div class="col-xs-1">
                                             <input type="checkbox" name="info" id="agree3"/>
                                            <label for="agree3"></label>
                                        </div>
                                        <div class="col-xs-11">
                                            <p> Akceptuję warunki Regulaminu i Polityki prywatności.
                                            </p>
                                        </div>
                                    </row>

                                    <row>
                                        <div class="col-xs-1">
                                             <input type="checkbox" name="info" id="agree4"/>
                                            <label for="agree4"></label>
                                        </div>
                                        <div class="col-xs-11">
                                            <p> Zapisz się do newslettera.
                                            </p>
                                        </div>
                                    </row>
                                </form>
                            </div>

                            <div>
                                <p>
                                <input class="button-3" name="login" value="Zarejestruj się" type="submit">
                                </p>
                            </div>

                        </form>
                    </div>
                </div>
            </div>

<?php include('footer.inc.php') ?>
