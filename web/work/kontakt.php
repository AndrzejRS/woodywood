<!-- tu inkludujemy header z pliku header.inc.php z tego samego katalogu -->
<?php include('header.inc.php') ?>

<!-- tu dajemy w każdej podstronce wszystko co ma się pokazać między headerem i footerem -->

        <div class="wrapper">


           <div class="container kontakt">
                <div>
                    <div class="col-sm-4">
                        <div class="info-box-map">
                            <div class="up-line"><p>Godebskiego, </p></div>
                            <div class="down-line"><p> 20-001 Lublin, Polska</p></div>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="info-box-map">
                            <div class="up-line"><p>+48 81 000 00 00</p></div>
                            <div class="down-line"><a href="#"><p>woodywood@home.pl</p></a>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-4">  
                        <div class="info-box-map">
                            <div class="up-line"><p>Godziny pracy</p></div>
                            <div class="down-line"><p>Pon - Pt: 09.00 do 17.00</p></div>
                        </div>
                    </div>
                    
                </div>
            </div>

            
            <div class="container kontakt kontakt-map">
                <div class="col-xs-12 col-md-6">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2497.368510701985!2d22.533531553158475!3d51.249123980364466!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x472259d8d6228c8f%3A0x3ad43b6126dbd5d2!2sCypriana+Godebskiego+1%2C+20-400+Lublin!5e0!3m2!1spl!2spl!4v1504815282030" width="400" height="400" frameborder="0" style="border:0" allowfullscreen>
                    </iframe>
                </div>
           
                <div  class="col-xs-12 col-md-6">
                     <form method="post" class="login login-kontakt">  
                            
                            <div class="name">
                                <p class="row-log">
                                <label for="name">Imię <span class="required"></span></label>
                                <input class="input-text-kontakt" name="name" id="name" value="" type="text" placeholder="Tutaj wpisz swoje imię">
                                </p>
                            </div>
                            
                            <div class="email">                        
                                <p class="row-log">
                                <label for="email">E-mail <span class="required"></span></label>
                                <input class="input-text-kontakt" name="email" id="email" type="email" placeholder="Tutaj wpisz swój e-mail">
                                </p>
                            </div>
                            
                            <div>
                                <textarea rows="6" cols="68" class="input-text-kontakt" id="comment"> Tutaj wpisz swój komentarz
                                </textarea>    
                            </div>

                            <div>
                                <p>
                                <input class="button-3-kontakt" name="sent" value="Wyślij" type="submit">
                                </p>
                            </div>

                        </form>
                </div>
            </div>

<?php include('footer.inc.php') ?>
       