<!DOCTYPE html>
    <html lang="pl">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta http-equiv="Content-type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="DESKRYPCJA">
        <meta name="keywords" content="KEYWORDS">
        <meta name="author" content="AUTHOR">
        
        <title>TYTUŁ STRONKI DO ZMIANY</title>
        <!--TU DAJEMY LINKOWANIE DO WSZYSTKIEGO -->

        <!-- link do google fonts-->
        <link href='http://fonts.googleapis.com/css?family=Play:400,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'> 
        <link href="https://fonts.googleapis.com/css?family=Josefin+Slab|Raleway" rel="stylesheet"> 

         <!-- tu podpinamy parser scss php -->
        <link rel="stylesheet" href="../scss.php?f=scss/style.scss" />
        <!-- jeśli są jakieś dodatkowe cssy to podpinamy je tak ale to możesz wywalić jeśli niema innych css -->
        <!-- slick dla zdjęć na indeksie fancybox -->
        <link rel="stylesheet" type="text/css" href="../css/jquery.fancybox.css" />
        <!-- slick dla podstrony referencje -->
        <link rel="stylesheet" href="../scss/slick.scss" />
        <link rel="stylesheet" href="../scss/slick-theme.scss" />
       
        <!-- Podpięcie Javascript i tu konieczny plug jquery trzeba go pobrać ze storny i umieścić w folderze js/vendor na dysku  --> 
        <script type="text/javascript" src="../js/vendor/jquery-2.2.4.min.js"></script>

       
        

    </head>
    <body>
        <div class="wrapper">
            <header class="navbar" role="navigation">
                <div class="container container-main">
                    <div class="header-top">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="log.php">Zaloguj się</a></li>
                            <li><a href="reg.php">Zarejestruj się</a></li>
                            <li><a href="koszyk.php">Koszyk</a></li>            
                        </ul>
                    </div>
                </div>

                <!-- znak logo i 3 x info-box -->
                    
                <div class="container container-custom hidden-xs hidden-sm">
                    <div class="header-middle">

                        <div class="col-sm-12">
                            <div class="logo-main">
                                <a href="index.php"><img src="#" alt="" title="logo">LOGO</a>
                            </div>
                        </div>
                        
                        
                    </div>
                </div>
    
                <!-- przycisk rozwijania menu -->
                
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse">
                            <span class="sr-only">Rozwiń menu</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                
                <!-- elementy menu -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse">
                        <ul class="nav navbar-nav navbar-custom">
                            <li class="active"><a href="onas.php">O nas</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Produkty<span class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="stoly.php">Stoły</a></li>
                                    <li><a href="krzesla.php">Krzesła</a></li>
                                    <li><a href="lustra.php">Lustra</a></li>
                                    <li><a href="polki.php">Półki</a></li>
                                    <li><a href="swieczniki.php">Świeczniki</a></li>                         
                                </ul>
                            </li>
                            <li><a href="referencje.php">Referencje</a></li>
                            <li><a href="kontakt.php">Kontakt</a></li>
                        </ul>
                    </div>
                </div>


            </header>